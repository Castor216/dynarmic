Source: dynarmic
Section: libs
Priority: optional
Maintainer: Andrea Pappacoda <andrea@pappacoda.it>
Build-Depends: catch2,
               cmake (>= 3.12),
               debhelper-compat (= 13),
               libboost1.81-dev,
               libfmt-dev,
               libxbyak-dev [any-amd64],
               libzydis-dev (>= 4.0.0) [any-amd64],
               llvm-dev,
               pkg-config,
               robin-map-dev,
               zlib1g-dev | libz-dev
Standards-Version: 4.6.2
Homepage: https://github.com/merryhime/dynarmic
Vcs-Git: https://salsa.debian.org/debian/dynarmic.git
Vcs-Browser: https://salsa.debian.org/debian/dynarmic
Rules-Requires-Root: no
Description: ARM dynamic recompiler
 Dynarmic is a dynamic recompiler for the ARMv6K, ARMv7A architecture, with
 partial ARMv8 support.
 .
 In the pursuit of speed, some behavior not commonly depended upon is elided.
 Therefore this emulator does not match spec.

Package: libdynarmic6.5
Architecture: any-amd64 any-arm64
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: ${source:Synopsis}
 ${source:Extended-Description}
 .
 This package contains the shared library.

Package: libdynarmic-dev
Section: libdevel
Architecture: any-amd64 any-arm64
Depends: libboost-dev,
         libdynarmic6.5 (= ${binary:Version}),
         llvm-dev,
         ${misc:Depends}
Description: ${source:Synopsis} - development
 ${source:Extended-Description}
 .
 This package contains the development files.
